import { v4 as uuid } from "uuid";
import * as AWS from "aws-sdk";

const kinesis = new AWS.Kinesis({ region: "us-west-2" });

const streamName = "SrEventsStreamStg";

const correlationId = uuid();

const srtest8 = {
  clientKey: "1b1dc032-9b43-31bb-9c34-199cc4500e2c",
  customerUrl: "https://srtest8.atlassian.net",
};

const srtest37 = {
  clientKey: "23b48500-77c7-3659-bb34-af19582e88d0",
  customerUrl: "https://srtest37.atlassian.net",
};

const srtest35 = {
  clientKey: "2e353aaa-8b8d-3147-a184-daf8893904b2",
  customerUrl: "https://srtest35.atlassian.net",
};

const srtest32 = {
  clientKey: "c3f29678-6c9f-3159-b42e-403956aab6da",
  customerUrl: "https://srtest32.atlassian.net",
};

const instance = srtest35;
const records = Array.from(Array(30).keys()).map((it) => {
  return {
    Data: Buffer.from(
      JSON.stringify({
        type: `event`,
        record: "unknown",
        clientKey: instance.clientKey,
        addonKey: "com.onresolve.jira.groovy.groovyrunner",
        correlationId:
          correlationId.slice(0, -10) + it.toString().padStart(10, "0"),
        atlassianUser: {
          accountId: "557058:98debc0e-06ba-4e71-baea-bbf1b7fd69ab",
        },
        customerUrl: instance.customerUrl,
      })
    ),
    PartitionKey: "event-processor-load-test",
  };
});

var params = {
  Records: records,
  StreamName: streamName,
};

Array.from(Array(100).keys()).forEach((it) => {
  kinesis.putRecords(params, function (err, data) {
    if (err) console.log(err, err.stack);
    // an error occurred
    else console.log(data); // successful response
  });
});